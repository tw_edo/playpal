package twiscode.playpal;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ApplicationData;

/**
 * Created by aldo on 19/04/2016.
 */
public class CommentActivity  extends AppCompatActivity {
    private TextView txtName;
    private ImageView imgView;
    private ApplicationData appData;
    private static LinearLayout btn_like;
    private static LinearLayout unlike;
    private static LinearLayout btn_going;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static EditText komen;
    private RecyclerViewHeader recyclerHeader;
    private EditText editText;
    private ImageView btn;
    private Toolbar toolbar;
    private android.support.v7.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_layout);

        EditText editText= (EditText) findViewById(R.id.editText);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        actionBar = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);




        imgView = (ImageView) findViewById(R.id.img_activity);
        Picasso.with(this).load(MyData.drawableArray[appData.itemPosition]).into(imgView);
        imgView = (ImageView) findViewById(R.id.img_activity);
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (imgView.getHeight() == 400) {
                    int width = imgView.getWidth();
                    imgView.setLayoutParams(new LinearLayout.LayoutParams(width, width));
                    imgView.setScaleType(ImageView.ScaleType.FIT_XY);

                } else {
                    int width = imgView.getWidth();
                    int height = imgView.getHeight();
                    imgView.setLayoutParams(new LinearLayout.LayoutParams(width, 400));
                    imgView.setScaleType(ImageView.ScaleType.FIT_XY);


                }
            }


        });

        btn_like = (LinearLayout) findViewById(R.id.btn_like);
        btn_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                LinearLayout unlike = (LinearLayout) findViewById(R.id.unlike);
                unlike.setVisibility(View.VISIBLE);
            }
        });

        unlike = (LinearLayout)findViewById(R.id.unlike);
        unlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                LinearLayout btn_like = (LinearLayout)findViewById(R.id.btn_like);
                btn_like.setVisibility(View.VISIBLE);
            }
        });

        btn_going = (LinearLayout)findViewById(R.id.btn_going);
        btn_going.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                view.setVisibility(view.GONE);
                LinearLayout done_going = (LinearLayout)findViewById(R.id.done_going);
                done_going.setVisibility(View.VISIBLE);
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager((this));
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<DataModel>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            data.add(new DataModel(
                    MyData.nameArray[i],
                    MyData.hourArray[i],
                    MyData.drawableArray[i],
                    MyData.dateArray[i],
                    MyData.commentArray[i]

            ));
        }

        adapter = new AdapterComment(data);
        recyclerHeader = (RecyclerViewHeader) findViewById(R.id.headerdetail);
        recyclerHeader.attachTo(recyclerView);
        recyclerView.setAdapter(adapter);
        // Inflate the layout for this fragment
        return ;
    }
    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        return true;

    }
}
