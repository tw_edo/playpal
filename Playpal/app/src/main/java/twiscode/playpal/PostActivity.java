package twiscode.playpal;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Created by aldo on 04/04/2016.
 */
public class PostActivity extends AppCompatActivity {
    private LinearLayout addpeople;
    private LinearLayout adddate;
    private int PICK_IMAGE_REQUEST = 1;
    private LinearLayout addphoto;
    private ImageView addphotocontainer;
    private View line_addphoto;
    public static TextView SelectedDateView;
    private static int Year;
    private static int Month;
    private static int Day;
    private static int hour;
    private static int Minute;
    public static String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post);
        SelectedDateView = (TextView) findViewById(R.id.selected_date);
        addpeople = (LinearLayout) findViewById(R.id.addpeople);

        addpeople.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(PostActivity.this, search_peopleActivity.class);

                startActivity(i);
                finish();
            }
        });

        addphoto = (LinearLayout) findViewById(R.id.addphoto);
        addphoto.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        addphotocontainer = (ImageView) findViewById(R.id.postImg);
        addphotocontainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        line_addphoto = (View) findViewById(R.id.line_addphoto);

    }

    public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
//                    SelectedDateView.setText("Selected Date: " + (month + 1) + "-" + day + "-" + year);
//            Toast.makeText(PostActivity.this,"Selected Date: " + (month + 1) + "-" + day + "-" + year, Toast.LENGTH_SHORT ).show();
            Day=day;
            Month=month;
            Year=year;
            showTimePickerDialog(view);
        }
    }
    public static TimePicker timePicker;

    public class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hour=hourOfDay;
            Minute=minute;
            setDateTime();
        }
    }
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }
    public void setDateTime(){
        SelectedDateView.setText("Selected Date: " + (MONTHS[Month]) + "-" + Day + "-" + Year + "-" + hour + "." + Minute);
//        Toast.makeText(PostActivity.this,"Selected Date: " + (Month + 1) + "-" + Day + "-" + Year + "-" + hour + "." + Minute, Toast.LENGTH_SHORT ).show();
    }


    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            addphoto.setVisibility(View.GONE);
            line_addphoto.setVisibility(View.GONE);
            addphotocontainer.setVisibility(View.VISIBLE);

            Picasso.with(this).load(selectedImage).into(addphotocontainer);
//                    addphotocontainer.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }
    }
    @Override
    public void onBackPressed() {
        Intent i = new Intent(PostActivity.this, MainActivity.class);

        startActivity(i);
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);

        finish();
}
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

}