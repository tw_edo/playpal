package twiscode.playpal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by aldo on 04/04/2016.
 */

public class search_peopleActivity extends AppCompatActivity {
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private ImageView btn;
    private ActionBar actionBar;
    private Toolbar toolbar;
    private Toolbar mActionBar;
    private static ArrayList<DataModel> data;{
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_people);
        final EditText input = (EditText)findViewById(R.id.title_text);

        recyclerView = (RecyclerView)findViewById(R.id.my_recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(search_peopleActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<DataModel>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            data.add(new DataModel(
                    MyData.nameArray[i],
                    MyData.hourArray[i],
                    MyData.dateArray[i],
                    MyData.drawableArray[i],
                    MyData.commentArray[i]
            ));
        }

        adapter = new SearchAdapter(data);

        recyclerView.setAdapter(adapter);
        // Inflate the layout for this fragment we

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        actionBar = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.actionbar_search, null);
        actionBar.setCustomView(mCustomView);
        actionBar.setDisplayShowCustomEnabled(true);

        btn = (ImageView) findViewById(R.id.cross);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(search_peopleActivity.this, PostActivity.class);

                startActivity(i);
                overridePendingTransition(R.anim.slide_out, R.anim.slide_in);

                finish();
            }
        });


    }




    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), PostActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(search_peopleActivity.this, PostActivity.class);

        startActivity(i);
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);

        finish();
    }

}

