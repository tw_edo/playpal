package twiscode.playpal;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by aldo on 05/04/2016.
 */
public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    private ArrayList<DataModel> dataSet;
    private Context context;

    public SearchAdapter(Context context, ArrayList<DataModel> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtHour;
        TextView txtComment;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.awijaya1);
            this.txtHour = (TextView) itemView.findViewById(R.id.aktivitas);
            this.txtComment = (TextView) itemView.findViewById(R.id.awijaya2);
        }
    }

    public SearchAdapter(ArrayList<DataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_layout, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView txtName = holder.txtName;
        TextView txtHour = holder.txtHour;
        TextView txtComment = holder.txtComment;

        txtName.setText(dataSet.get(listPosition).getName());
        txtHour.setText(dataSet.get(listPosition).getHour());
        txtComment.setText(dataSet.get(listPosition).getComment());

//        Picasso.with(context).load(dataSet.get(listPosition).getImage()).into(viewHolder.img_android);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }}
