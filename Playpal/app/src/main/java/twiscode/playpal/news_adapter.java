package twiscode.playpal;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class news_adapter extends RecyclerView.Adapter<news_adapter.MyViewHolder> {

    private List<news_model> newsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nama, alamat, jumlahlike, jumlahcomment, waktu;

        public MyViewHolder(View view) {
            super(view);
            nama = (TextView) view.findViewById(R.id.nama);
            alamat = (TextView) view.findViewById(R.id.alamat);
            jumlahlike = (TextView) view.findViewById(R.id.jumlahlike);
            jumlahcomment= (TextView) view.findViewById(R.id.jumlahcomment);
            waktu= (TextView) view.findViewById(R.id.waktu);
        }
    }


    public news_adapter(List<news_model> newsList) {
        this.newsList = newsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_rows, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        news_model movie = newsList.get(position);
        holder.nama.setText(movie.getNama());
        holder.alamat.setText(movie.getAlamat());
        holder.jumlahlike.setText(movie.getJumlahlike());
        holder.jumlahcomment.setText(movie.getJumlahcomment());
        holder.waktu.setText(movie.getWaktu());
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}