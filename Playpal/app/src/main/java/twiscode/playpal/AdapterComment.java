package twiscode.playpal;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;



/**
 * Created by aldo on 19/04/2016.
 */
public class AdapterComment extends RecyclerView.Adapter<AdapterComment.MyViewHolder> {
    private ArrayList<DataModel> dataSet;
    private static Context context;
    private static LinearLayout btn_like;
    private static LinearLayout unlike;

    public AdapterComment(Context context, ArrayList<DataModel> dataSet) {
        this.context = context;
        this.dataSet = dataSet;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtHour;
        TextView txtComment;


        public MyViewHolder(final View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.pertama1);
            this.txtHour = (TextView) itemView.findViewById(R.id.kedua);
            this.txtComment = (TextView) itemView.findViewById(R.id.ketiga);

            }}

    public AdapterComment(ArrayList<DataModel> data) {
        this.dataSet = data;
    }

@Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_comment, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        TextView txtName = holder.txtName;
        TextView txtHour = holder.txtHour;
        TextView txtComment = holder.txtComment;

        txtName.setText(dataSet.get(listPosition).getName());
        txtHour.setText(dataSet.get(listPosition).getHour());
        txtComment.setText(dataSet.get(listPosition).getComment());

//        Picasso.with(context).load(dataSet.get(listPosition).getImage()).into(holder.imgView);
    }

@Override
    public int getItemCount() {
        return dataSet.size();
    }

//@Override
//    public long getItemId(int position) {
//        return position;
//    }


}
