package twiscode.playpal.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import twiscode.playpal.DataModel;
import twiscode.playpal.R;

/**
 * Created by User on 4/15/2016.
 */
public class AdapterFollowReq extends RecyclerView.Adapter<AdapterFollowReq.MyViewHolder> {

    private static ArrayList<DataModel> dataSet;
    private static Context context;
    private static ImageView accept;
    private static ImageView reject;
    private static Button btn_after;
    private static Button button_follow;
    private static RelativeLayout tiga;
    private static Button btn_after4;
    private static Button btn_after3;

    public AdapterFollowReq(Context context, ArrayList<DataModel> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtHour;
        TextView txtComment;

        public MyViewHolder(final View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.nama);
            this.txtHour = (TextView) itemView.findViewById(R.id.following);
            this.txtComment = (TextView) itemView.findViewById(R.id.join);
            accept = (ImageView) itemView.findViewById(R.id.accept);
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    btn_after = (Button) itemView.findViewById(R.id.btn_after);
                    btn_after.setVisibility(View.VISIBLE);
                }
            });
            reject = (ImageView) itemView.findViewById(R.id.reject);
            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    RelativeLayout after5 = (RelativeLayout) itemView.findViewById(R.id.after5);
                    after5.setVisibility(View.VISIBLE);
                    View viewtiga = (View) itemView.findViewById(R.id.viewtiga);
                    viewtiga.setVisibility(View.VISIBLE);
                    RelativeLayout tiga = (RelativeLayout) itemView.findViewById(R.id.tiga);
                    tiga.setVisibility(View.GONE);
                    View empat = (View) itemView.findViewById(R.id.empat);
                    empat.setVisibility(View.GONE);
                }
            });
            btn_after = (Button) itemView.findViewById(R.id.btn_after);
            btn_after.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    Button btn_after3 = (Button) itemView.findViewById(R.id.btn_after3);
                    btn_after3.setVisibility(View.VISIBLE);
                }
            });
            btn_after3 = (Button) itemView.findViewById(R.id.btn_after3);
            btn_after3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(R.string.app_name);
                    builder.setMessage("unfollow " + dataSet.get(getAdapterPosition()).getName() + "?");
                    builder.setPositiveButton("Yes, of course", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            view.setVisibility(View.GONE);
                            Button btn_after3 = (Button) itemView.findViewById(R.id.btn_after);
                            btn_after3.setVisibility(View.VISIBLE);


                        }
                    });
                    builder.setNegativeButton("Cancel", null);
                    builder.show();

                }
            });
            button_follow = (Button) itemView.findViewById(R.id.button_follow);
            button_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    Button btn_after4 = (Button) itemView.findViewById(R.id.btn_after4);
                    btn_after4.setVisibility(View.VISIBLE);
                }
            });

            btn_after4 = (Button) itemView.findViewById(R.id.btn_after4);
            btn_after4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(R.string.app_name);
                    builder.setMessage("unfollow " + dataSet.get(getAdapterPosition()).getName() + "?");
                    builder.setPositiveButton("Yes, of course", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            view.setVisibility(View.GONE);
                            Button btn_after4 = (Button) itemView.findViewById(R.id.button_follow);
                            btn_after4.setVisibility(View.VISIBLE);


                        }
                    });
                    builder.setNegativeButton("Cancel", null);
                    builder.show();

                }
            });
        }
    }


    public AdapterFollowReq(ArrayList<DataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout_follow, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView txtName = holder.txtName;
        TextView txtHour = holder.txtHour;
        TextView txtComment = holder.txtComment;

        txtName.setText(dataSet.get(listPosition).getName());
        txtHour.setText(dataSet.get(listPosition).getHour());
        txtComment.setText(dataSet.get(listPosition).getComment());

//        Picasso.with(context).load(dataSet.get(listPosition).getImage()).into(viewHolder.img_android);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }}