    package twiscode.playpal;

/**
 * Created by User on 4/5/2016.
 */
public class User {
    private String nama;
    private String profpic;
    private String email;
    private String birthday;
    private String id;

    public User(String id, String nama, String profpic, String email, String birthday) {
        this.nama = nama;
        this.profpic = profpic;
        this.email = email;
        this.birthday = birthday;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getProfpic() {
        return profpic;
    }

    public void setProfpic(String profpic) {
        this.profpic = profpic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }




}