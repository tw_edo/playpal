package twiscode.playpal;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;

import java.util.ArrayList;

/**
 * Created by aldo on 29/03/2016.
 */
public class ThreeFragment extends Fragment {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;
    private RecyclerViewHeader recyclerHeader;
    private static Button button_follow;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = null;
////        button_follow = (Button) root.findViewById(R.id.button_follow);
////        button_follow.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                Intent i = new Intent(getContext(), edit_profile.class);
//
//                startActivity(i);
//
//            }
//        });

        root = inflater.inflate(R.layout.fragment_three, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.my_recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<DataModel>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            data.add(new DataModel(
                    MyData.nameArray[i],
                    MyData.hourArray[i],
                    MyData.drawableArray[i],
                    MyData.dateArray[i],
                    MyData.commentArray[i]
            ));
        }


        recyclerHeader = (RecyclerViewHeader) root.findViewById(R.id.header);
        adapter = new Layout3Adapter(getContext(), data);
        recyclerHeader.attachTo(recyclerView);

        recyclerView.setAdapter(adapter);
        // Inflate the layout for this fragment
        return root;
    }



}

