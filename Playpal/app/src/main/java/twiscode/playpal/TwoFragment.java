package twiscode.playpal;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;

import java.util.ArrayList;

import twiscode.playpal.Fragment.FragmentFollowReq;

/**
 * Created by aldo on 29/03/2016.
 */
public class TwoFragment extends Fragment implements View.OnClickListener {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;
    private RecyclerViewHeader recyclerHeader;
    private RelativeLayout show_follow_req;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_two, container, false);

        show_follow_req = (RelativeLayout) root.findViewById(R.id.show_follow_req);
        show_follow_req.setOnClickListener(this);

        recyclerView = (RecyclerView) root.findViewById(R.id.my_recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<DataModel>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            data.add(new DataModel(
                    MyData.nameArray[i],
                    MyData.hourArray[i],
                    MyData.drawableArray[i],
                    MyData.dateArray[i],
                    MyData.commentArray[i]

            ));
        }

        adapter = new Layout2Adapter(data);
        recyclerHeader = (RecyclerViewHeader) root.findViewById(R.id.header);
        recyclerHeader.attachTo(recyclerView);
        recyclerView.setAdapter(adapter);
        // Inflate the layout for this fragment
        return root;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.show_follow_req){
            FragmentTransaction transaction = getFragmentManager()
                    .beginTransaction();
            /*
             * When this container fragment is created, we fill it with our first
             * "real" fragment
             */
            transaction.replace(R.id.fragment_two_container, new FragmentFollowReq());
            transaction.commit();
        }
    }
}



