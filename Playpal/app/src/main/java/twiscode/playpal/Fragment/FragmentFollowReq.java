package twiscode.playpal.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;

import java.util.ArrayList;

import twiscode.playpal.Adapter.AdapterFollowReq;
import twiscode.playpal.DataModel;
import twiscode.playpal.MyData;
import twiscode.playpal.R;
import twiscode.playpal.TwoFragment;
import twiscode.playpal.Utilities.ApplicationData;

/**
 * Created by User on 4/15/2016.
 */
public class FragmentFollowReq extends Fragment implements View.OnClickListener {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;
    private RecyclerViewHeader recyclerHeader;
    private RelativeLayout show_follow_req;
    private static ImageView accept;
    private static Button btn_after;



    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtHour;
        TextView txtComment;
        ImageView imgView;
        ApplicationData appData;
        private LinearLayoutManager layoutManager;

        public MyViewHolder(final View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.awijaya);
            this.txtHour = (TextView) itemView.findViewById(R.id.at15_00);
            this.txtComment = (TextView) itemView.findViewById(R.id.comment);
            this.imgView = (ImageView) itemView.findViewById(R.id.img_activity);

            accept = (ImageView) itemView.findViewById(R.id.accept);
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    btn_after = (Button) itemView.findViewById(R.id.btn_after);
                    btn_after.setVisibility(View.VISIBLE);
                }
            });
            btn_after = (Button) itemView.findViewById(R.id.btn_after);
            btn_after.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    Button btn_after = (Button) itemView.findViewById(R.id.btn_after);
                    btn_after.setVisibility(View.VISIBLE);
                }
            });
            btn_after = (Button) itemView.findViewById(R.id.btn_after);
            btn_after.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    Button btn_after = (Button) itemView.findViewById(R.id.btn_after);
                    btn_after.setVisibility(View.VISIBLE);
                }
            });

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_follow_req, container, false);


        recyclerView = (RecyclerView) root.findViewById(R.id.my_recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<DataModel>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            data.add(new DataModel(
                    MyData.nameArray[i],
                    MyData.hourArray[i],
                    MyData.drawableArray[i],
                    MyData.dateArray[i],
                    MyData.commentArray[i]

            ));
        }

        adapter = new AdapterFollowReq(getContext(),data);

        recyclerView.setAdapter(adapter);
        // Inflate the layout for this fragment
        return root;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    FragmentTransaction transaction = getFragmentManager()
                            .beginTransaction();

                    /*
                     * When this container fragment is created, we fill it with our first
                     * "real" fragment
                     */
                    transaction.replace(R.id.fragment_two_container, new TwoFragment());

                    transaction.commit();

                    return true;

                }

                return false;
            }
        });
    }
}

